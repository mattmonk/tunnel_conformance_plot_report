<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
<!ENTITY rarr     "&#8594;" >
]>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:xsd12d="http://www.12d.com/schema/xml12d-10.0" 
				exclude-result-prefixes="xsl xsd12d" >
	<!-- HTML5 -->
	<xsl:output
		encoding="UTF-8"
		indent="yes"
		method="html"
		omit-xml-declaration="yes" />
		
	<xsl:decimal-format decimal-separator="." grouping-separator="," NaN="N/A" />	
	
	<!-- #### GENERIC REPORT SETTINGS #### -->
	<!-- Include company logo
		 0 = disabled, 1 = enabled -->
	<xsl:variable name="USE_COMPANY_LOGO">1</xsl:variable>
	<!-- Include project details in header
		 1 = enabled, otherwise = disabled -->
	<xsl:variable name="PROJECT_DETAILS">1</xsl:variable>
 
	<!-- #### STYLING OF REPORTS #### -->
	<!-- Uses UPPERCASE headings in the table output
		 Otherwise, headings will be Proper case.
		 0 = disabled, 1 = enabled -->
	<xsl:variable name="UPPER_CASE_HEADINGS">1</xsl:variable>
	<!-- Use alternating row colours
		 0 = disabled, 1 = enabled -->
	<xsl:variable name="ALTERNATE_ROW_COLOURS">1</xsl:variable>
	<!-- Include over/under text columns
		 0 = disabled, 1 = enabled -->
	<xsl:variable name="SHOW_CONFORM_RESULT_COLUMN">1</xsl:variable>
	<!-- Show legend for colouring of cells
		 0 = disabled, 1 = enabled -->
	<xsl:variable name="SHOW_COLOURING_LEGEND">1</xsl:variable>
	
	<!-- ### CONFORMANCE TOLERANCES ### -->
	<!-- CONFORMANCE EXACT MATCH -->
	<!-- When the conformance matches exactly -->
	<!-- Whether to show the result row
			0 = disabled, 1 = enabled -->
	<xsl:variable name="CONFORM_EXACT_SHOW_ROW">1</xsl:variable>
	<!-- Whether to show the result value
			0 = disabled, 1 = enabled -->
	<xsl:variable name="CONFORM_EXACT_SHOW_RESULT">1</xsl:variable>
	<!-- Text to display in result text column -->
	<xsl:variable name="CONFORM_EXACT_TEXT">OK</xsl:variable>
	<!-- CSS class name to assign to conformance result cell (for styling) -->
	<xsl:variable name="CONFORM_EXACT_CLASS">conform_exact</xsl:variable>
	
	<!-- CONFORMANCE UNDER OK -->
	<!-- When the conformance is under, but within tolerance -->
	<!-- Whether to show the result row
			0 = disabled, 1 = enabled -->
	<xsl:variable name="CONFORM_UNDER_OK_SHOW_ROW">1</xsl:variable>
	<!-- Whether to show the result value
			0 = disabled, 1 = enabled -->
	<xsl:variable name="CONFORM_UNDER_OK_SHOW_RESULT">1</xsl:variable>
	<!-- Text to display in result text column -->
	<xsl:variable name="CONFORM_UNDER_OK_TEXT">OK</xsl:variable>
	<!-- CSS class name to assign to conformance result cell (for styling) -->
	<xsl:variable name="CONFORM_UNDER_OK_CLASS">conform_under_ok</xsl:variable>
		
	<!-- CONFORMANCE OVER OK -->
	<!-- When the conformance is over, but within tolerance -->	
	<!-- Whether to show the result row
			0 = disabled, 1 = enabled -->
	<xsl:variable name="CONFORM_OVER_OK_SHOW_ROW">1</xsl:variable>
	<!-- Whether to show the result value
			0 = disabled, 1 = enabled -->
	<xsl:variable name="CONFORM_OVER_OK_SHOW_RESULT">1</xsl:variable>
	<!-- Text to display in result text column -->
	<xsl:variable name="CONFORM_OVER_OK_TEXT">OK</xsl:variable>
	<!-- CSS class name to assign to conformance result cell (for styling) -->
	<xsl:variable name="CONFORM_OVER_OK_CLASS">conform_over_ok</xsl:variable>
		
	<!-- CONFORMANCE UNDER NOT OK -->
	<!-- When the conformance is under, but within tolerance -->
	<!-- Whether to show the result row
			0 = disabled, 1 = enabled -->	
	<xsl:variable name="CONFORM_UNDER_NOT_OK_SHOW_ROW">1</xsl:variable>
	<!-- Whether to show the result value
			0 = disabled, 1 = enabled -->
	<xsl:variable name="CONFORM_UNDER_NOT_OK_SHOW_RESULT">1</xsl:variable>
	<!-- Text to display in result text column -->
	<xsl:variable name="CONFORM_UNDER_NOT_OK_TEXT">TOO THIN</xsl:variable>
	<!-- CSS class name to assign to conformance result cell (for styling) -->
	<xsl:variable name="CONFORM_UNDER_NOT_OK_CLASS">conform_under_not_ok</xsl:variable>
	
	<!-- CONFORMANCE OVER NOT OK -->
	<!-- When the conformance is over, but within tolerance -->	
	<!-- Whether to show the result row
			0 = disabled, 1 = enabled -->
	<xsl:variable name="CONFORM_OVER_NOT_OK_SHOW_ROW">1</xsl:variable>
	<!-- Whether to show the result value
			0 = disabled, 1 = enabled -->
	<xsl:variable name="CONFORM_OVER_NOT_OK_SHOW_RESULT">1</xsl:variable>
	<!-- Text to display in result text column -->
	<xsl:variable name="CONFORM_OVER_NOT_OK_TEXT">TOO THICK</xsl:variable>
	<!-- CSS class name to assign to conformance result cell (for styling) -->
	<xsl:variable name="CONFORM_OVER_NOT_OK_CLASS">conform_over_not_ok</xsl:variable>
		
	<!-- #### FORMATTING VARIABLES #### -->
	<!-- Formatting pattern for the format-number() function:
		0 = Digit
		# = Digit, zeros are not shown
		. = The position of the decimal point Example: ###.##
		, = The group separator for thousands. Example: ###,###.##
		% = Displays the number as a percentage. Example: ##%
		; = Pattern separator. The first pattern will be used for positive numbers and the second for negative numbers
	-->
	<!-- Generic formatting for 3 decimal places -->
	<xsl:variable name="format_3dp">0.000</xsl:variable>
	<!-- Formatting pattern for coordinates -->
	<xsl:variable name="format_coord">0.000</xsl:variable>
	<!-- Formatting pattern for chainage -->
	<xsl:variable name="format_chg">0.000</xsl:variable>
	<!-- Formatting pattern for clearance -->
	<xsl:variable name="format_clearance">0.000</xsl:variable>
	<!-- Formatting pattern for size -->
	<xsl:variable name="format_size">0.000</xsl:variable>
	<!-- Formatting pattern for element index -->
	<xsl:variable name="format_index">00</xsl:variable>
	<xsl:variable name="format_distance">0.000</xsl:variable>
	<!-- Formatting pattern for percentage -->
	<xsl:variable name="format_percentage">0.0</xsl:variable>
	<!-- Formatting pattern for depth -->
	<xsl:variable name="format_depth">0.000</xsl:variable>
	
	<!-- ### COMPANY LOGO ### -->
	<xsl:template name="company_logo">
		<div class="company_logo">
			<xsl:if test="$USE_COMPANY_LOGO = 1">
				<!-- You can either point to a logo image on the internet or embed one locally
						Embedding is not recommended for any large icons ( > 15 KB).
						To embed, we need to convert the binary image file and encode it into Base64 encoding.
						There are several websites that can do this, such as http://base64image.org/ 
					
						Refer to examples and documentation online for how to embed the image.
						
						Don't forget to specify the height and width of your image!
				-->
				<img alt="12d Cube logo" width="50" height="62" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAA+CAYAAABp/UjKAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAABl0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMC4yMfEgaZUAAAvDSURBVGhD7Zp5UFRXFocx20zKqfknk4w6mZnMmAwBE2VRgwIiIIKySgOiICi4oIAKGBdQO4CCgBA17htG4wIRFTSaqKhBEBFoRFyQRTal2ekFFTXdvznvQTsNPKAbNWLVnKqvmup37+vzvXvPvRdojf9HL2HC57/jGOpn6BTmG8ILXfCVs7Pz2+2X3pxw5i8Y5BTut8EpzK+UF+73xCnct4h+5jtE+H7Q3qT/h2OYnwMv3LeQkn9GMlDQJuSXz1vjb9/etP+FFd//z1NCfS14Yb6pJCFTFuDG9zQv3H+se0zQwPZbvN5wig143zHU35bq4BBJiLiT7pZaXpjfXupn48nn/7H9lr9vMIXrstbf1GXtwhP0Wu2yxl9OP0NtmH7hTP+FSS4RC0fx+fy32j/i1Yd7UNDAaREBUdMjA+qmRS6W0SteFPY+EYsfuEYErHil08050fntWRtXfOgRvcTdIyqomMArY11goWdMkOuMiOUfvNQR8okJ+mh2XLCPd+zyDO/1y54R+B1gPufc7Nhgj7nrA//SnkrfIoAK2ScuxHP+hpC0eXHBYp9vg/EaaPSJC/7VZ2PIDP+N/n9oT03FgMYAv03Bo/2/W53qt2m1xG/TKjn9jNcF8/kE5bE6ZdHWlZqU4YC2RLsJfiL/vSWbowcFbg3jB2wJbSbQD6kL2BK24uvdUUPm7tjxbnvqbUEF9U7wzjU6y3ZFfrN0+9qSpTvX4g2gZNnOyGAS0nROTGw7w204uicmZHfUreA9UU9D9kTjTSF4d9QTehXEHNkeyU9MfE+juLKsdWvyAfC/j3ujWL0vFit3rMP0Jd4P3YPcB2pIpS2ob2zE+ZzLiPtxJyIOfYc1P2zs14TGr0dgTAjs57vCynsKTBY4/4kVUVBdV4Nz2WnYceoHRCdsQ9SRLf2KsPhYBKwPwbRAL1ZAQRcRBrFEgnsPKnE+Nx1bUr5H3NFdr52YhO1YEseHa5AXJs/hdZDoVkRBs0iEqppqnMpMxebkfdh0Iv53Z+OxPVi7Jw5TaQRs5jl3EVDQo4gCsViCm6WFOJh6HDt/Oojtpw68crad3I+4I9sxl78I1nOdOJNXRiURBXWNDci6LUBS+hnEn03A7p8Pv3SYB8UILP2WD56/O2fSXKglokBYXwdBUQErtD816aXw/fmj2HJsLxZHrYBrwCzOZHuiTyIMzILACGXeESAh7SQO/ZrcZw5cSMKaXbFwWezJWciq0GcRBRKJFNX1tbiYn4FjV84gKeO0yiTSA9h6dC9mLJ3bZwEFPYtQkYtEYu5rnRCJxSisLMGlG5k4lX0eyVlnu4WR2JVyAAHrVsB2ngusvCiZXpg8mxF15LzG0EVEQsk3N4voSbeg5sRpFG/dg5KC22hsau6QeHc0NDWi+H4Z0m5mIeXauQ4CzIhtS4rHonXL4USFbDnLQSXs57oicnssfFYt4rzO0EWkatte3I78FndO/owCWxfkffolcl29UFZe0SHh3mCEbpXfxZnci6zE4QsnEBQdgikL3DBxpgMsPO17xcrLEbazXbDzUDxqaPrOW7mIsx2DiXMnkbLVkRD8bSgEf/8MgsH/guCv/0TueGuUlpZ1SFQVmPppbG5CVoEATn4k4GkLCw8blbD0tMNUXw8knEzC49bHkLRIYU3Ti6stQxcRYUIyCgxMkPfxp6xEm8jkPokoyMrLwYQZtmphMcMOvqsCkVuQh99kMuTdusHZTkEXEYYmqony8Gjka+txijQVFqN870GUJBxHeWERGhoaO/TvDCNi5matNuOcLXHy3GnISGRf0g+cbRRwijCIHwhx29Kui4iksYnen4K8z6h2tHQhMLfF9QMJqKq6z06lzvdhyBKQyLTJamE+3Qb23lORfu0KmFgWuZKznQITZxNuEUlDE+5YO3YRYVYyweB/P592gkGfQDB0GHJpdauuFna5D8NVQTbGu1qphfl0a/gsX4ibhbcgl8thP8eFs50CtUUqwmLakqfF4LrBeNzQN2TFcvWNkJ+UzDnNGBGTqZYqw0jYzOJhVUw4HgirUV5V0fbUOdo+R10RIdXFTQMz5JpboywrBw3Xb6JkfgArl+0+G3cF+V2m2NXcbBg7WajERDdbBIWtQELKUaRlpqP1yROknPsJ412sONsrUFuEqZGG7Dw8OH4KTTV17HvNRSW4MdyAptgXuPbjcTRSG+V7ZeZeg5HjhF6xmG4L78D5uHn3DtZsXCc/df4MWx8Rm2NgzLPg7KNAbZHniCT/a1vfiLvTvNi211ZHoLKiqkNbRsTQ0bxHjHiUjJMlUs6eRmrGRTjOdq1POfsTWx+egXM5+yjTdxElJFQXRR7z2LbZnj4ovlvU4XpmDok4mPWICW8iAvnLUPmgCgHffA1j+wl3rtIDYHZ0B28Xzj7KvJCIRCxFfVEpKvcnIH/YyDYRl5kopr1FuV1mThbG2Jv2iN1MJ+xLOIB7FWWY5Gb/dLyDhZT5VTsjOxOWbnacfZTpk4iETsTVdBYrnLMQBVT0eZo6bDtWZH4gqsorO9zrSnYWDGzH94jtDB7KKstx7EwKjOzNS3y+9mPr40DSIZmpsxVnH2X6JFIZsxnXae8QDFHaT5h2/9BE/uEfIe509GdEvrIx6RbrGY6YQ0Uuk8uwbvN6jDAbXZh06oScLfSNUcKwuIinY+xMOfsq6FZE2ixG6aJlyP+cnraZDe7dK2ffr7uY/vwwyTJkKPJI6oaBKYpoU2yuq+9yr4zsqxg12bhbmNFITG47HC5YsfixvsWYJ0102Hz48KFsc/x2cUDoMtlo63GcfRVom3QnwkAydb9cRMWGbaivb2DfK/Vb2rYhErmmVri7ei0qjyaj6V73x/yMayQyyYiT0ZTEyImGqKq+zxb2zIVzykLXrxUJa4RoIZEzqb/ILFxtOPsq07OIAqWpcnuSU9tIkEjORHvcoN28p3MWAyOibzmWEzMnK4yzm8AeDGvqap9Fb46rjf4u9tnDR4/YvwuErOOzslx9lVFNRAlhwgncGEHHEnZaUY0wh0efQNxidvluTsEZWZnQsxjDCTOtnLymsfvFb7Lf5PWNDTIHD5e6loctsoPHEmBkZ87ZrzNqi0jpyYvuV6M0aCXVD51+B7dPs1HjkLsjHuV3i9k/6Cn3SScR3QkGnFi7OcDF240VYUblyIlEjLWhVcjBAvoWYzn7cKEQuUVTQ6b84b0hYern3CWULFhEI0THE2aEaNVidvaKso71wojomH/FyShLIxhMGldfVlH+6NKVNNh7OnO26xaz0b/R600DA4P3NVpaWnSJ9STTpJyAKohr61F3IY09OF7/RIv2kSCUl7atcArSr2ZihOmobvly/MhmnrfrYytXOzY5rjZcDDcdVT/CbGTYcPORuhoaGuy/sQcIhcKBtNyNlkqlycpJqIq4ugbCw8dQvnM/aoW1Ha5dvnqFSfblYjLyl2HjRo5iR4IjBtAe9LZEInEQi6W3aYSeKifUK8zKxbF6sSIm+i/OOP0nxPUvTPQd9PX1O/4jtLug0fmYptsqGqFragt14nLmFQwz1usz2oa6z7SN9XK0jfWXDx8z/KP2FFUPGp23WltbP6MRWiCRtBSQkJwr0d64nJkBbSPdviDXNtLJ1TYc4fufMV98Tim92Nc5cnJy3hWLxZoks4lkHnMl2xN9EjHUbdUeqxszbKzeUJWnkarB1A9NNz0SukBCT1QdITVE5KyAoU6alpHeq/8OJAm9K5U+cqckL5FMS+fEO6OKiJaRzkMSOK9lrDtVW1v7vfaPevVBMgOYBUEkkrrRgnCahJ5xSTD0KGKo80zLUDd1mKGOx+fGeoPp1j1/z+RVBbMg0HQbRHiRULmaIkUk4qk5VneIBv8FC/llBSMkEomG0sjsJ1qV66eTiFyLHQWdw5omIz7pNwJc0XZCaEki6hmhdhG5lpFuLS2nx2glMm1v2v+DTrMDabrZ0Ap38OLltPvaxroJNAqThlsM7x9fj1UnmAVBLJZ/kJqeqqljpPMhvfV6CvnNCg2N/wKdk5VHsWdzmQAAAABJRU5ErkJggg==" />
			</xsl:if>
		</div>
	</xsl:template>
	
	<xsl:variable name="report_parameters" select="/xsd12d:xml12d/xsd12d:report/xsd12d:tunnel_conformance/xsd12d:parameters" />
	<xsl:variable name="under_tolerance" select="$report_parameters/xsd12d:tun_under_tol" />
	<xsl:variable name="over_tolerance" select="$report_parameters/xsd12d:tun_over_tol" />
		
	<xsl:template match="xsd12d:meta_data"></xsl:template>	
	
	<!-- ### PROJECT DETAILS ### -->
	<xsl:template name="project_details">
		<xsl:variable name="meta_data" select="/xsd12d:xml12d/xsd12d:meta_data" />
		<xsl:if test="$PROJECT_DETAILS = 1">
			<table class="parameters">
				<tbody>
					<tr>
						<td colspan="2" class="details_section_heading">General</td>
					</tr>
					<tr>
						<td class="details_title">Project:</td>
						<td class="details_value">
							<xsl:value-of select="$meta_data/xsd12d:application/xsd12d:project_name" />
						</td>
					</tr>
					<tr>
						<td class="details_title">Directory:</td>
						<td class="details_value">
							<xsl:value-of select="$meta_data/xsd12d:application/xsd12d:project_folder" />
						</td>
					</tr>
					<tr>
						<td class="details_title">User:</td>
						<td class="details_value">
							<xsl:value-of select="$meta_data/xsd12d:application/xsd12d:user" />
						</td>
					</tr>
					<tr>
						<td class="details_title">Created:</td>
						<td class="details_value">
							<xsl:call-template name="fix_timestamp">
								<xsl:with-param name="timestamp" select="$meta_data/xsd12d:application/xsd12d:export_date" />
							</xsl:call-template>
						</td>
					</tr>
					<tr>
						<td class="details_title">Software:</td>
						<td class="details_value">
							<xsl:value-of select="$meta_data/xsd12d:application/xsd12d:application" />
						</td>
					</tr>
				</tbody>
			</table>
		</xsl:if>		
	</xsl:template>
	
	<!-- ### PARAMETERS ### -->
	<xsl:template match="xsd12d:parameters">
		<div class="report_parameters">
			<div class="params_wrapper">
				<div class="params_table" id="params_general">
					<xsl:call-template name="project_details" />
				</div>
			
				<div class="params_table" id="params_tunnel">
					<table class="parameters">
						<tr>
							<td colspan="2" class="details_section_heading">Tunnel</td>
							</tr>
						<tr>
							<td class="details_title">Conformance Type:</td>
							<td>
								<xsl:value-of select="xsd12d:conform_type_text" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Tunnel Definition 1:</td>
							<td>
								<xsl:value-of select="xsd12d:tun_file_path" /><xsl:value-of select="xsd12d:tun_file_name" />
							</td>
							</tr>
						<tr>
							<td class="details_title">Tunnel Definition 2:</td>
							<td>
								<xsl:value-of select="xsd12d:tun_file_path" /><xsl:value-of select="xsd12d:tun_file_name" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Chg Type</td>
							<td><xsl:value-of select="xsd12d:tun_ch_type_text" /></td>
						</tr>
						<tr>
							<td class="details_title">Profile Type</td>
							<td><xsl:value-of select="xsd12d:tun_prf_type_text" /></td>
						</tr>
						<tr>
							<td class="details_title">Control String</td>
							<td><xsl:value-of select="xsd12d:cs_model" />&rarr;<xsl:value-of select="xsd12d:cs_name" /></td>
						</tr>
						<tr>
							<td class="details_title">Setout Offset</td>
							<td><xsl:value-of select="format-number(xsd12d:so_tun_ele_os, $format_3dp)" /></td>
						</tr>
						<tr>
							<td class="details_title">Under Tolerance</td>
							<td><xsl:value-of select="format-number(xsd12d:tun_under_tol, $format_3dp)" /></td>
						</tr>
						<tr>
							<td class="details_title">Over Tolerance</td>
							<td><xsl:value-of select="format-number(xsd12d:tun_over_tol, $format_3dp)" /></td>
						</tr>
						<tr>
							<td class="details_title">Under is negative?</td>
							<td><xsl:value-of select="xsd12d:tun_under_neg" /></td>
						</tr>
					</table>				
				</div>
				<div class="params_table" id="params_survey">
					<table class="parameters">
						<tr>
							<td colspan="2" class="details_section_heading">Survey Info</td>
						</tr>
						<tr>
							<td class="details_title">Surveyor</td>
							<td><xsl:value-of select="xsd12d:surveyors_name" /></td>
						</tr>
						<tr>
							<td class="details_title">Location</td>
							<td><xsl:value-of select="xsd12d:job_lot_number" /></td>
						</tr>
						<tr>
							<td class="details_title">State</td>
							<td><xsl:value-of select="xsd12d:job_category" /></td>
						</tr>
						<tr>
							<td class="details_title">Description</td>
							<td><xsl:value-of select="xsd12d:job_description" /></td>
						</tr>
						<tr>
							<td class="details_title">Instrument</td>
							<td>
								<xsl:value-of select="xsd12d:instrument_type_as_text" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Serial #</td>
							<td>
								<xsl:choose>
									<xsl:when test="xsd12d:instrument_serial_number = -1">
										<xsl:text>N/A</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xsd12d:instrument_serial_number" />
									</xsl:otherwise>
								</xsl:choose>								
							</td>
						</tr>
						<tr>
							<td class="details_title">Survey Time</td>
							<td><xsl:value-of select="xsd12d:pc_local_time_text" /></td>
						</tr>
					</table>
				</div>				
				<div class="params_table" id="params_legend">
					<xsl:call-template name="legend" >
						<xsl:with-param name="parameters" select="." />
					</xsl:call-template>				
				</div>				
			</div>
		</div>
		<br /><br />
	</xsl:template>
	
	<!-- ### TO UPPERCASE ### -->
	<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
	<xsl:template name="change_case">
		<xsl:param name="value" />
		<xsl:choose>
			<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
				<xsl:value-of select="translate($value, $lowercase, $uppercase)" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$value" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	
	
	<xsl:template name="table_headings">
		<thead>
			<tr>
				<th rowspan="2">
					<xsl:call-template name="change_case">
						<xsl:with-param name="value">Pt</xsl:with-param>
					</xsl:call-template>
				</th>					
				<th>	
					<xsl:call-template name="change_case">
						<xsl:with-param name="value">Ctrl Str Chg</xsl:with-param>
					</xsl:call-template>
				</th>
				<th rowspan="2">
					<xsl:call-template name="change_case">
						<xsl:with-param name="value">Profile Chg</xsl:with-param>
					</xsl:call-template>
				</th>
				<th colspan="3">
					<xsl:call-template name="change_case">
						<xsl:with-param name="value">Element</xsl:with-param>							
					</xsl:call-template>
				</th>
				<th rowspan="2">
					<xsl:call-template name="change_case">
						<xsl:with-param name="value">Thickness</xsl:with-param>
					</xsl:call-template>
				</th>
				<th rowspan="2">
					<xsl:call-template name="change_case">
						<xsl:with-param name="value">Outside Tolerance</xsl:with-param>
					</xsl:call-template>
				</th>
				<xsl:if test="$SHOW_CONFORM_RESULT_COLUMN = 1">
					<th rowspan="2">
					<xsl:call-template name="change_case">
						<xsl:with-param name="value">Conformance Result</xsl:with-param>
					</xsl:call-template>
					</th>
				</xsl:if>
			</tr>
			<tr>		
				<th>
					<xsl:choose>
						<xsl:when test="ancestor::xsd12d:tunnel_conformance/xsd12d:parameters/xsd12d:tun_ch_type_text = '2d'">
							<xsl:call-template name="change_case">
								<xsl:with-param name="value">2d</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="ancestor::xsd12d:tunnel_conformance/xsd12d:parameters/xsd12d:tun_ch_type_text = '3d'">
							<xsl:call-template name="change_case">
								<xsl:with-param name="value">3d</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
					</xsl:choose>
				</th>
				<th>
					<xsl:call-template name="change_case">
						<xsl:with-param name="value">Name</xsl:with-param>
					</xsl:call-template>
				</th>
				<th>
					<xsl:call-template name="change_case">
						<xsl:with-param name="value">Distance (m)</xsl:with-param>
					</xsl:call-template>
				</th>					
				<th>
					<xsl:call-template name="change_case">
						<xsl:with-param name="value"> Distance (%)</xsl:with-param>
					</xsl:call-template>
				</th>
			</tr>
		</thead>
	</xsl:template>
	
	<xsl:template name="chg_result_summary">
		<xsl:param name="chg_result" />
		<xsl:variable name="num_under_not_ok" select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state = -2])" />
		<xsl:variable name="num_under_ok"     select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state = -1])" />
		<xsl:variable name="num_exact"        select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state =  0])" />
		<xsl:variable name="num_over_ok"      select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state =  1])" />
		<xsl:variable name="num_over_not_ok"  select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state =  2])" />
		<xsl:variable name="num_within_tol"   select="($num_under_ok + $num_exact + $num_over_ok)" />
		<xsl:variable name="num_total_pts"    select="count($chg_result/xsd12d:point_result)" />
		<div style="display:inline-block">
			<div class="params_wrapper">
				<xsl:element name="div">
					<xsl:attribute name="class">
						<xsl:text>params_table chg_result_state </xsl:text>
						<xsl:choose>
							<xsl:when test="$num_under_not_ok &gt; 0">
								<xsl:value-of select="$CONFORM_UNDER_NOT_OK_CLASS" />
							</xsl:when>
							<xsl:when test="$num_over_not_ok &gt; 0">
								<xsl:value-of select="$CONFORM_OVER_NOT_OK_CLASS" />
							</xsl:when>
							<xsl:when test="$num_within_tol = $num_total_pts">
								<xsl:value-of select="$CONFORM_EXACT_CLASS" />
							</xsl:when>
						</xsl:choose>
					</xsl:attribute>
					<xsl:call-template name="chg_result_state">
						<xsl:with-param name="chg_result" select="$chg_result" />
					</xsl:call-template>
				</xsl:element>
				<div class="params_table">
					<table class="parameters">
						<tr>
							<td colspan="2" class="details_section_heading">Number of Points</td>
							</tr>
						<tr>
							<td class="details_title">Tight</td>
							<td class="text-right">
								<xsl:value-of select="$num_under_not_ok" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Within Tolerance</td>
							<td class="text-right">
								<xsl:value-of select="$num_within_tol" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Over</td>
							<td class="text-right">
								<xsl:value-of select="$num_over_not_ok" />
							</td>
						</tr>
						<tr>
							<td class="details_title">TOTAL</td>
							<td class="text-right">
								<xsl:value-of select="$num_total_pts" />
							</td>
						</tr>
					</table>
				</div>		
				<div class="params_table">
					<table class="parameters">
						<tr>
							<td colspan="2" class="details_section_heading">Thicknesses</td>
							</tr>
						<tr>
							<td class="details_title">Min. Thickness:</td>
							<td class="text-right">
								<xsl:value-of select="format-number($chg_result/xsd12d:min_offset, $format_depth)" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Max. Thickness:</td>
							<td class="text-right">
								<xsl:value-of select="format-number($chg_result/xsd12d:max_offset, $format_depth)" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Mean Thickness:</td>
							<td class="text-right">
								<xsl:value-of select="format-number($chg_result/xsd12d:mean_offset, $format_depth)" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Std Dev Thickness:</td>
							<td class="text-right">
								<xsl:value-of select="format-number($chg_result/xsd12d:sdev_offset, $format_depth)" />
							</td>
						</tr>
					</table>
				</div>
				<div class="params_table">
					<table class="parameters">
						<tr>
							<td colspan="2" class="details_section_heading">
								<xsl:text>Ctrl Str Chg </xsl:text>
									<xsl:choose>
										<xsl:when test="ancestor::xsd12d:tunnel_conformance/xsd12d:parameters/xsd12d:tun_ch_type_text = '2d'">
											<xsl:text>(2d)</xsl:text>
										</xsl:when>
										<xsl:when test="ancestor::xsd12d:tunnel_conformance/xsd12d:parameters/xsd12d:tun_ch_type_text = '3d'">
											<xsl:text>(3d)</xsl:text>
										</xsl:when>
									</xsl:choose>
							</td>
						</tr>
						<tr>
							<td class="details_title">Min. Chainage:</td>
							<td class="text-right">
								<xsl:value-of select="format-number($chg_result/xsd12d:min_cs_raw_2d_ch, $format_chg)" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Max. Chainage:</td>
							<td class="text-right">
								<xsl:value-of select="format-number($chg_result/xsd12d:max_cs_raw_2d_ch, $format_chg)" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Mean Chainage:</td>
							<td class="text-right">
								<xsl:value-of select="format-number($chg_result/xsd12d:mean_cs_raw_2d_ch, $format_chg)" />
							</td>
						</tr>
						<tr>
							<td class="details_title">Std Dev Chainage:</td>
							<td class="text-right">
								<xsl:value-of select="format-number($chg_result/xsd12d:sdev_cs_raw_2d_ch, $format_chg)" />
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="xsd12d:results" >
		
		<table class="parameters">
			<thead>
				<tr class="details_section_heading">
					<td colspan="2">Summary of Results</td>
				</tr>
				<tr>
					<th>Chainage</th>
					<th>Result</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="xsd12d:result">
					<xsl:sort select="xsd12d:nominal_chainage" data-type="number" order="ascending" />
					<xsl:variable name="nom_chg" select="format-number(xsd12d:nominal_chainage, $format_chg)" />
					<tr>
						<td>
							<xsl:element name="a">
								<xsl:attribute name="href">
									<xsl:text>#</xsl:text>
									<xsl:value-of select="$nom_chg" />
								</xsl:attribute>
								<xsl:value-of select="$nom_chg" />
							</xsl:element>
						</td>
						<xsl:element name="td">	
							<xsl:attribute name="class">
								<xsl:text>text-center </xsl:text>
								<xsl:call-template name="chg_result_class">
									<xsl:with-param name="chg_result" select="." />
								</xsl:call-template>
							</xsl:attribute>
							<xsl:call-template name="chg_result_state" >
								<xsl:with-param name="chg_result" select="." />
							</xsl:call-template>
						</xsl:element>
					</tr>
				</xsl:for-each>
			</tbody>				
		</table>
		<br />
		<hr />
		<br />
		<xsl:apply-templates select="xsd12d:result" />
	</xsl:template>
	
	<!-- ### CONFORMANCE RESULTS ### -->
	<xsl:template match="xsd12d:result">
		<xsl:variable name="nom_chg" select="format-number(xsd12d:nominal_chainage, $format_chg)" />
		<xsl:element name="h2">
			<xsl:attribute name="id">
				<xsl:value-of select="$nom_chg" />
			</xsl:attribute>
			<xsl:text>CHG </xsl:text>
			<xsl:value-of select="$nom_chg" />
		</xsl:element>
		<xsl:call-template name="chg_result_summary">
			<xsl:with-param name="chg_result" select="." />
		</xsl:call-template>
		<table class="setout">
			<xsl:call-template name="table_headings" />
			<tbody>
				<xsl:apply-templates select="xsd12d:point_result" />
			</tbody>
		</table>
		<br />
		<a href="#">&#9650; Top</a>
		<hr />
	</xsl:template>
		
	<!-- ### CONFORMANCE POINT RESULT ROW ### -->
	<xsl:template match="xsd12d:point_result">
		<xsl:variable name="print_result_row_ok">
			<xsl:call-template name="print_row_ok">
				<xsl:with-param name="pt_result" select="." />
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$print_result_row_ok = 1">
			<tr>
				<td class="pt_num"><xsl:value-of select="format-number( count(preceding-sibling::*[name() = name(current())]) + 1, $format_index)" /></td>
				<td class="ctrl_chg">
					<!-- Control String Chainage (2d) -->
					<xsl:choose>
						<xsl:when test="ancestor::xsd12d:tunnel_conformance/xsd12d:parameters/xsd12d:tun_ch_type_text = '2d'">
							<xsl:value-of select="format-number(xsd12d:pu_cs_raw_2d_ch, $format_chg)" />
						</xsl:when>
						<xsl:when test="ancestor::xsd12d:tunnel_conformance/xsd12d:parameters/xsd12d:tun_ch_type_text = '3d'">
							<xsl:value-of select="format-number(xsd12d:pu_cs_raw_3d_ch, $format_chg)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>N/A</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</td>
				<td class="prof_chg"><xsl:value-of select="format-number(xsd12d:pu_tun_prf_ch, $format_chg)" /></td>
				<td class="elem_name">
					<xsl:value-of select="xsd12d:pu_tun_ele_name" />
				</td>
				<!-- Profile Element Index (zero-based) -->
				<!--
				<td class="elem_index">
					<xsl:value-of select="format-number((xsd12d:pu_tun_ele_idx + 1), $format_index)" />
				</td> -->
				<!-- Profile Element Distance -->
				<td class="elem_dist">
					<xsl:value-of select="format-number(xsd12d:pu_tun_ele_dist, $format_distance)" />
				</td>
				<!-- Profile Element Ratio (%) -->
				<td class="elem_percent">
					<xsl:value-of select="format-number( (xsd12d:pu_tun_ele_per * 100), $format_percentage)" />
				</td>
				<!-- Conformance Distance -->
				<xsl:call-template name="conform_thickness">
					<xsl:with-param name="pt_result" select="." />
				</xsl:call-template>
				<!-- Conformance Outside Tolerance -->
				<td class="conform_delta">
					<xsl:choose>
						<xsl:when test="not(xsd12d:pu_tun_out_of_tol = 'null')">
							<xsl:value-of select="format-number(xsd12d:pu_tun_out_of_tol, $format_depth)" />
						</xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>
				</td>
				<xsl:if test="$SHOW_CONFORM_RESULT_COLUMN = 1">
					<td class="result_text">
						<xsl:call-template name="conform_result_text">
							<xsl:with-param name="pt_result" select="." />
						</xsl:call-template>
					</td>
				</xsl:if>
			</tr>
		</xsl:if>
	</xsl:template>

	<xsl:template name="chg_result_state">
		<xsl:param name="chg_result" />
		<xsl:variable name="num_under_not_ok" select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state = -2])" />
		<xsl:variable name="num_under_ok"     select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state = -1])" />
		<xsl:variable name="num_exact"        select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state =  0])" />
		<xsl:variable name="num_over_ok"      select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state =  1])" />
		<xsl:variable name="num_over_not_ok"  select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state =  2])" />
		<xsl:variable name="num_within_tol">
			<xsl:choose>
				<xsl:when test="ancestor::xsd12d:parameters/xsd12d:tun_under_tol and ancestor::xsd12d:parameters/xsd12d:tun_over_tol">
					<xsl:value-of select="($num_exact + $num_under_ok + $num_over_ok)" />
				</xsl:when>
				<xsl:when test="ancestor::xsd12d:parameters/xsd12d:tun_under_tol">
					<xsl:value-of select="($num_exact + $num_over_ok + $num_over_not_ok )" />
				</xsl:when>
				<xsl:when test="ancestor::xsd12d:parameters/xsd12d:tun_over_tol">
					<xsl:value-of select="($num_exact + $num_under_ok + $num_under_not_ok) " />
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="num_total_pts"    select="count($chg_result/xsd12d:point_result)" />
		<xsl:choose>
			<xsl:when test="($num_under_not_ok &gt; 0)">
				<xsl:text>FAIL</xsl:text>
			</xsl:when>
			<xsl:when test="($num_over_not_ok &gt; 0)">
				<xsl:text>OVER</xsl:text>
			</xsl:when>
			<xsl:when test="$num_within_tol = $num_total_pts">
				<xsl:text>PASS</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>????</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="chg_result_class">
		<xsl:param name="chg_result" />
		<xsl:variable name="num_under_not_ok" select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state = -2])" />
		<xsl:variable name="num_under_ok"     select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state = -1])" />
		<xsl:variable name="num_exact"        select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state =  0])" />
		<xsl:variable name="num_over_ok"      select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state =  1])" />
		<xsl:variable name="num_over_not_ok"  select="count($chg_result/xsd12d:point_result[xsd12d:tun_result_state =  2])" />
		<xsl:variable name="num_within_tol"   select="($num_under_ok + $num_exact + $num_over_ok)" />
		<xsl:variable name="num_total_pts"    select="count($chg_result/xsd12d:point_result)" />
		<xsl:choose>
			<xsl:when test="$num_under_not_ok &gt; 0">
				<xsl:value-of select="$CONFORM_UNDER_NOT_OK_CLASS" />
			</xsl:when>
			<xsl:when test="$num_over_not_ok &gt; 0">
				<xsl:value-of select="$CONFORM_OVER_NOT_OK_CLASS" />
			</xsl:when>
			<xsl:when test="$num_within_tol = $num_total_pts">
				<xsl:value-of select="$CONFORM_EXACT_CLASS" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	
	<!-- ### CONFORMANCE DISTANCE ### -->
	<xsl:template name="conform_dist">
		<xsl:param name="pt_result" />
		<xsl:variable name="dist" select="( $pt_result/xsd12d:pu_tun_ele_depth * -1 )" />
		<xsl:variable name="state" select="$pt_result/xsd12d:tun_result_state" />
		<xsl:element name="td">
			<xsl:attribute name="class">conform_dist</xsl:attribute>
			<xsl:choose>
				<xsl:when test="$state = -2">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_UNDER_NOT_OK_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_UNDER_NOT_OK_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:when test="$state = -1">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_UNDER_OK_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_UNDER_OK_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:when test="$state = 0">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_EXACT_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_EXACT_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:when test="$state = 1">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_OVER_OK_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_OVER_OK_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:when test="$state = 2">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_OVER_NOT_OK_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_OVER_NOT_OK_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="conform_thickness">
		<xsl:param name="pt_result" />
		<xsl:variable name="dist" select="$pt_result/xsd12d:tun_trimesh_depth" />
		<xsl:variable name="state" select="$pt_result/xsd12d:tun_result_state" />
		<xsl:element name="td">
			<xsl:attribute name="class">conform_dist</xsl:attribute>
			<xsl:choose>
				<xsl:when test="$state = -2">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_UNDER_NOT_OK_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_UNDER_NOT_OK_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:when test="$state = -1">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_UNDER_OK_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_UNDER_OK_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:when test="$state = 0">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_EXACT_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_EXACT_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:when test="$state = 1">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_OVER_OK_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_OVER_OK_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:when test="$state = 2">
					<xsl:attribute name="class">
						<xsl:value-of select="$CONFORM_OVER_NOT_OK_CLASS" />
					</xsl:attribute>
					<xsl:if test="$CONFORM_OVER_NOT_OK_SHOW_RESULT = 1">
						<xsl:value-of select="format-number($dist, $format_depth)" />
					</xsl:if>
				</xsl:when>
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	
	
	<!-- ### PRINT ROW OR NOT ### -->
	<!-- Tests whether the current row should be printed or not
		 based on the current state of the conformance result and
		 the user-specified settings at the top of this XSLT.
		 Returns:
			0 - Do not print row
			1 - Print row
	-->
	<xsl:template name="print_row_ok">
		<xsl:param name="pt_result" />
		<xsl:variable name="state" select="$pt_result/xsd12d:tun_result_state" />
		<xsl:choose>
			<xsl:when test="$state = -2 and $CONFORM_UNDER_NOT_OK_SHOW_ROW = 1">1</xsl:when>
			<xsl:when test="$state = -1 and $CONFORM_UNDER_OK_SHOW_ROW = 1">1</xsl:when>
			<xsl:when test="$state =  0 and $CONFORM_EXACT_SHOW_ROW = 1">1</xsl:when>
			<xsl:when test="$state =  1 and $CONFORM_OVER_OK_SHOW_ROW = 1">1</xsl:when>
			<xsl:when test="$state =  2 and $CONFORM_OVER_NOT_OK_SHOW_ROW = 1">1</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- ### CONFORMANCE RESULT TEXT ### -->
	<xsl:template name="conform_result_text">
		<xsl:param name="pt_result" />
		<xsl:variable name="state" select="$pt_result/xsd12d:tun_result_state" />
		<xsl:choose>
			<xsl:when test="$state = -2">
				<xsl:value-of select="$CONFORM_UNDER_NOT_OK_TEXT" />
			</xsl:when>
			<xsl:when test="$state = -1">
				<xsl:value-of select="$CONFORM_UNDER_OK_TEXT" />
			</xsl:when>
			<xsl:when test="$state = 0">
				<xsl:value-of select="$CONFORM_EXACT_TEXT" />
			</xsl:when>
			<xsl:when test="$state = 1">
				<xsl:value-of select="$CONFORM_OVER_OK_TEXT" />
			</xsl:when>
			<xsl:when test="$state = 2">
				<xsl:value-of select="$CONFORM_OVER_NOT_OK_TEXT" />
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
			
	<!-- ### HTML CSS STYLE ### -->
	<xsl:template name="css_style">
		<style type="text/css">
		@media print {
			body {
				font-size:	8pt;
			}			
		}
		
		body {
			font-family:	Arial, sans-serif;
			font-size:		small;
			margin:			0.5em;
		}
		
		caption {
			font-size:	120%;
			font-weight: bold;
			padding:	1em;
		}
		
		table {
			border-spacing:		0;
			border-collapse:	collapse;
		}
		
		th, td {
			padding:			0.25em 0.5em;	/* Padding of cells in data rows of all tables
													Overridden for setout tables below */
		}
		
		.report_parameters {
			display:			inline-block;
			margin-bottom:		20px;
		}
		.params_wrapper {
			display:			block;
		}
		.params_table {
			float:				left;
			padding-left:		10px;
			padding-right:		10px;
		}
		
		.details_section_heading {
			background-color:	#ccc;
			text-align:			center;
			font-weight:		bold;
		}
		
		.details_title {
			font-weight:		bold;
			min-width:			100px;
		}
		
		table.setout {
			background-color:	#F2F2F2;		/* General background colour of setout tables */
			border-style:		solid;
			border-width:		1px;
			border-color:		black;
			page-break-inside:	auto;
		}
		
		table.setout tr {
			page-break-inside:	avoid;
			page-break-after:	auto;
			-webkit-region-break-inside: avoid;
		}
		
		table.setout th {
			color:				white;			/* Font colour for header row */
			background-color:	#777777;		/* Background color for header row of setout tables */
			font-weight:		bold;			/* Font weight for header row of setout tables */
			padding:			0.25em 0.5em;	/* Padding of cells in header row of setout tables
													This uses the shorthand CSS property.
													The 1st value is the top and bottom padding.
													The 2nd value is the left and right padding.
												*/
			border-style:		solid;			/*	Border style around header row cells */
			border-width:		1px;			/*	Border thickness around header row cells */
			border-color:		black;			/*	Border colour around header row cells */
		}
		
		table.setout td {
			padding:			0.25em 0.5em;	/* Padding of cells in data rows of setout tables
													This uses the shorthand CSS property.
												*/
			border-style:		solid;			/*	Border style around data cells */
			border-width:		1px;			/*	Border thickness around data cells */
			border-color:		black;			/*	Border colour around data cells */
		}

		<xsl:if test="$ALTERNATE_ROW_COLOURS = 1">
		table.setout tr:nth-child(even) {
			background-color:	#ccc;			/* Background colour for even rows */
		}
		table.setout tr:nth-child(odd) {
			/* background-color:	#fff; */			/* Background colour for odd rows
													Comment out to use default table background colour */
		}
		</xsl:if>
		
		.<xsl:value-of select="$CONFORM_EXACT_CLASS" /> {
			background-color:		#00ff00;
			text-align:				right;
		}
		.<xsl:value-of select="$CONFORM_UNDER_NOT_OK_CLASS" /> {
			background-color:		red;
			text-align:				right;
		}
		.<xsl:value-of select="$CONFORM_UNDER_OK_CLASS" /> {
			background-color:		#00ff00;
			text-align:				right;
		}
		.<xsl:value-of select="$CONFORM_OVER_OK_CLASS" /> {
			background-color:		#00ff00; /* #d9d900; */
			text-align:				right;
		}
		.<xsl:value-of select="$CONFORM_OVER_NOT_OK_CLASS" /> {
			background-color:		#ffff00;
			text-align:				right;
		}
	
		/* Alignment of columns */
		.pt_num, .elem_name, .elem_index, .result_text {
			text-align:		center;
		}
		.prof_chg, .elem_dist, .elem_percent, .ctrl_chg, .conform_dist, .conform_delta {
			text-align:		right;
		}
		.text-left {
			text-align:		left;
		}
		.text-right {
			text-align:		right;
		}
		.text-center {
			text-align:		center;
		}	
		
		#header {
			margin-bottom:		20px;
		}
		
		#header div {
			display:	inline-block;	
		}
		
		.company_logo {
			vertical-align:		middle;
		}
		
		.company_logo img {
		}
		
		.report_header {
			margin-left:		20px;
		}
		
		#footer {
			font-size:	x-small;
			margin-top:	20px;
			text-align: center;
		}
		
		.chg_result_state {
			text-align:		center;
			vertical-align:	middle;
			line-height:	100px;
			width:		100px;
			height:		100px;
			max-width:	100px;
			max-height:	100px;
			font-size:		xx-large;
			font-weight:	bold;
		}
		</style>
	</xsl:template>
	
	<!-- ### CONFORMANCE COLOUR LEGEND ### -->
	<!-- Relies on and uses the same classes used to colour the cells. -->
	<xsl:template name="legend">
		<xsl:param name="parameters" />
		<xsl:if test="$SHOW_COLOURING_LEGEND = 1">
			<xsl:choose>
				<xsl:when test="$parameters/xsd12d:tun_under_tol or $parameters/xsd12d:tun_over_tol">
					<table class="parameters">
						<tr>
							<td colspan="2" class="details_section_heading">Legend</td>					
						</tr>
					<xsl:if test="$parameters/xsd12d:tun_under_tol">
						<tr>
							<xsl:element name="td">
								<xsl:attribute name="class">
									<xsl:value-of select="$CONFORM_UNDER_NOT_OK_CLASS" />
								</xsl:attribute>
								&lt; <xsl:value-of select="format-number($parameters/xsd12d:tun_under_tol, $format_depth)" />				
							</xsl:element>
							<td>Too Thin</td>
						</tr>
					</xsl:if>
					<xsl:if test="$parameters/xsd12d:tun_under_tol and $parameters/xsd12d:tun_over_tol">
						<tr>
							<xsl:element name="td">
								<xsl:attribute name="class">
									<xsl:value-of select="$CONFORM_EXACT_CLASS" />
								</xsl:attribute>
								<xsl:value-of select="format-number($parameters/xsd12d:tun_under_tol, $format_depth)" />
								<xsl:text> - </xsl:text>
								<xsl:value-of select="format-number($parameters/xsd12d:tun_over_tol, $format_depth)" />
							</xsl:element>
							<td>OK</td>
						</tr>
					</xsl:if>
					<xsl:if test="$parameters/xsd12d:tun_over_tol">
						<tr>
							<xsl:element name="td">
								<xsl:attribute name="class">
									<xsl:value-of select="$CONFORM_OVER_NOT_OK_CLASS" />
								</xsl:attribute>
								&gt; <xsl:value-of select="format-number($parameters/xsd12d:tun_over_tol, $format_depth)" />		
							</xsl:element>
							<td>Too Thick</td>
						</tr>
					</xsl:if>
					</table>
				</xsl:when>
				<xsl:otherwise>
				<!-- No table -->	
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	
	<!-- ### FIX TIMESTAMP ### -->
	<!-- Replaces the T and Z in an ISO-8601 timestamp with a space -->
	<xsl:template name="fix_timestamp">
		<xsl:param name="timestamp" />
		<xsl:value-of select="translate($timestamp, 'TZ', ' ')" />		
	</xsl:template>
		
	<!-- ### MAIN TEMPLATE ### -->
	<xsl:template match="/">
		<!-- HTML5 Doc type -->
		<xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;
		</xsl:text>
		<html>
			<head>
				<title>Thickness Tunnel Conformance Report - 12d Model</title>
				<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				<xsl:call-template name="css_style" />
			</head>
			<body>
				<xsl:for-each select=".">
					<div id="header">
					<!-- ### REPORT LOGO ### -->
					<xsl:call-template name="company_logo" />							
					<div class="report_header"><h1>Thickness Tunnel Conformance Report</h1></div>
					</div>
					<xsl:apply-templates />
				</xsl:for-each>
				<!-- Footer -->
				<div id="footer">Generated by 12d Model at 
				<xsl:call-template name="fix_timestamp">
					<xsl:with-param name="timestamp" select="xsd12d:xml12d/xsd12d:meta_data/xsd12d:application/xsd12d:export_date" />
				</xsl:call-template>.  
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
